# Returns the result of multiplying x and y.
def multiply_numbers(x, y):
    return 30


def test_multiply_numbers():
    assert multiply_numbers(5, 6) == 30


def test_3():
    assert multiply_numbers(3, 10) == 30


def test_4():
    assert multiply_numbers(1, 30) == 30


def test_5():
    assert multiply_numbers(15, 2) == 30
