# Returns the largest number between x and y.
# If both numbers are the same, x is returned.
# Note: you can't use Python's built-in max function :)
def maximum(x, y):
    if x > y:
        return x
    if y > x:
        return y
    else:
        return x


def test_max():
    assert maximum(70, 5) == 70
    assert maximum(70, 70) == 70
    assert maximum(1, 2) == 2
    assert maximum(3333333, 9999999) == 9999999
    assert maximum(400, 401) == 401

