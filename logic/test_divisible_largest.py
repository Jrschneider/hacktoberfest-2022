# Given integer number <num>, return True if the number is cleanly divisible by its largest digit.
# Example: is_divisible_largest_digit(123) is True, because 123 can be divided by 3.
import pytest


def is_divisible_largest_digit(num):
    if num < 0:
        num = abs(num)
    largest = 0
    for i in str(num):
        if int(i) > largest:
            largest = int(i)
    if int(num) % largest == 0:
        return True
    return False


def test_is_divisible_largest_digit_integers():
    assert is_divisible_largest_digit(-123)
    assert is_divisible_largest_digit(-37303)
    assert is_divisible_largest_digit(37303)
    assert not is_divisible_largest_digit(47)


def test_is_divisible_largest_digit_for_exceptions():
    with pytest.raises(ZeroDivisionError):
        is_divisible_largest_digit(0)
    with pytest.raises(TypeError):
        is_divisible_largest_digit("fr")
