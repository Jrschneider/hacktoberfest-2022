# You are given collection <elements>, which is composed of integer numbers.
# Given an index factor, <n>, return the sum of all values in <elements> where their index (1-indexed) is a factor of n
def sum_nth_index_elements(elements, n):
    finalNum = 1
    for i in elements:
        if i % n == 0 and i != 0:
            finalNum = finalNum * i

    return finalNum


def test_sum_nth_index_elements():
    assert sum_nth_index_elements([1, 2, 3, 4, 5, 6], 2) == 2 * 4 * 6
    assert sum_nth_index_elements([1, 2, 3, 4, 5, 6, 7, 8, 9], 3) == 3 * 6 * 9
    assert sum_nth_index_elements([0, 1, 2, 3, 4, 5, 6, 7, 8], 4) == 4 * 8
    assert sum_nth_index_elements([-4, -2, 0, 1, 2, 6, 7, 8], 2) == -4 * -2 * 2 * 6 * 8
    assert sum_nth_index_elements([0.50, 1, 1.25, 1.69, 2.99, 3.00, 6], 0.25) == 0.50 * 1 * 1.25 * 3.00 * 6

