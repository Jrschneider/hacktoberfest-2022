# Given string <base> and <substring>, return the number of times <substring> appears in <base>.
def count_substring(base, substring):
    count = base.count(substring)
    return count


def test_count_substring():
    assert count_substring("abcabcabc", "abc") == 3
    assert count_substring("apples", "hotdog") == 0
    assert count_substring("schlichting","ch") == 2
    assert count_substring("aaaaaaaaaa","a") == 10
    assert count_substring("12345","8") == 0