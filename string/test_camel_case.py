# Converts and returns the input to camelCaseFormatting.
# In camelCase, the first character is underscored, and characters after spaces
# are uppercase.

from re import sub
def to_camel_case(input):
    input = sub(r"(_|-)", " ", input).title().replace(" ", "")
    return ''.join([input[0].lower(), input[1:]])


def test_snake_case():
    assert to_camel_case("Hello world how are you") == "helloWorldHowAreYou"
