# Given input <string>, return True if the amount of whitespace on the left-hand side of a center character is the
# same as the amount of whitespace on the right-hand side of the same character.
# For an empty string, return False.
# Example: is_centered(" D ") is True, because the center 'D' is surrounded by an equal amount of whitespace.
def is_centered(string):
    left = 0
    right = 0
    for i in range(string.__len__()):

        if string[i] == " ":
            left = left + 1
        else:
            start = i+1
            break
    for i in range(start, string.__len__()):
        if string[i] == " ":
            right = right + 1
        else:
            break
    if right == left:
        return True
    else:
        return False


def test_is_centered():
    assert is_centered(" D ")
    assert is_centered("     X     ")
    assert not is_centered(" @     ")
